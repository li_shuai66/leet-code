import java.io.*;

public class Ted_Util {
    public static void main(String[] args) throws IOException {
        //创建字符输入对象，关联数据文件
        FileReader fr = new FileReader("C:\\Users\\Sunshine\\Desktop\\笔记\\ted\\first.txt");
        //创建字符输出流对象， 关联目的地文件
        FileWriter fw = new FileWriter("C:\\Users\\Sunshine\\Desktop\\笔记\\ted\\second.txt");
        // 定义变量，记录读取到的内容
        int len ;
        //循环读取，只要条件满足就一直读，并将读取到的内容赋值给变量
        while ((len = fr.read())!=-1 ){
            // 将读取到的数据写入到  目的地文件中
            fw.write(len);
            if(len=='.')
                fw.write('\n');
        }
        //释放资源
        fr.close();
        fw.close();
    }
}
