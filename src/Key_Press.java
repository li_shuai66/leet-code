import java.awt.*;
import java.awt.event.KeyEvent;

public class Key_Press {
    public static void main(String[] args) throws Exception {
        Robot r=new Robot();//创建自动化工具对象
        int i=1;
        while (true)
        {
            if(i%4==0){
                r.keyPress(KeyEvent.VK_LEFT);
                Thread.sleep(500);
                r.keyRelease(KeyEvent.VK_LEFT);
            }else if(i%4==1){
                r.keyPress(KeyEvent.VK_UP);
                Thread.sleep(500);
                r.keyRelease(KeyEvent.VK_UP);
            }else if(i%4==2){
                r.keyPress(KeyEvent.VK_RIGHT);
                Thread.sleep(500);
                r.keyRelease(KeyEvent.VK_RIGHT);
            }else {
                r.keyPress(KeyEvent.VK_UP);
                Thread.sleep(500);
                r.keyRelease(KeyEvent.VK_UP);
            }
            i++;
        }
    }
}
