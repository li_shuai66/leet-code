package com.ls.que76_80;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：ls
 * @date ：2021/11/30 10:42
 * @description：...
 */
public class Subsets {
    public static void search(List<List<Integer>> res, List<Integer> temp, boolean[] flag, int num, int k, int[] nums){
        temp.add(nums[num-1]);
        flag[num] = true;
        k -= 1;
        if (k==0){
            List<Integer> a = new ArrayList<>();
            a.addAll(temp);
            res.add(a);
            temp.remove(temp.size()-1);
            flag[num] = false;
            return;
        }
        for (int i=num+1; i<=nums.length;i++){
            if (!flag[i]){
                search(res, temp, flag, i, k, nums);
            }
        }

        temp.remove(temp.size()-1);
        flag[num] = false;
    }

    public static List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        List<Integer> temp = new ArrayList<>();
        boolean[] flag = new boolean[nums.length+1];
        for (int k=1; k<=nums.length; k++){
            for (int i=1; i<=nums.length; i++){
                search(res, temp, flag, i, k, nums);
            }
        }
        res.add(new ArrayList<Integer>());
        return res;
    }

    public static void main(String[] args) {
        int[] nums = new int[]{0};
        List<List<Integer>> res = subsets(nums);
        for (List<Integer> r : res){
            for (int a : r){
                System.out.print(a + " ");
            }
            System.out.println();
        }
    }
}
