package com.ls.que76_80;

/**
 * @author ：ls
 * @date ：2021/12/2 10:33
 * @description：...
 */
public class RemoveDuplicates {
    public static int removeDuplicates(int[] nums) {
        if (nums.length == 1)
            return 1;
        if (nums.length == 2){
            return 2;
        }
        int i=1,j=2;
        while (j < nums.length){
            if (nums[j] != nums[i] ||(nums[j] == nums[i] && nums[j] != nums[i-1])){
                nums[++i] =nums[j];
            }
            j++;
        }
        return i+1;
    }
}
