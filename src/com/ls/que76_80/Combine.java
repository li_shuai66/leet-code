package com.ls.que76_80;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：ls
 * @date ：2021/11/30 10:03
 * @description：...
 */
public class Combine {

    public static void search(List<List<Integer>> res, List<Integer> temp, boolean[] flag, int n, int num, int k){
        temp.add(num);
        flag[num] = true;
        k -= 1;
        if (k==0){
            List<Integer> a = new ArrayList<>();
            a.addAll(temp);
            res.add(a);
            temp.remove(temp.size()-1);
            flag[num] = false;
            return;
        }
        for (int i=num+1; i<=n;i++){
            if (!flag[i]){
                search(res, temp, flag, n, i, k);
            }
        }

        temp.remove(temp.size()-1);
        flag[num] = false;
    }

    public static List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> res = new ArrayList<>();
        List<Integer> temp = new ArrayList<>();
        boolean[] flag = new boolean[n+1];
        for (int i=1; i<=n; i++){
            search(res, temp, flag, n, i, k);
        }
        return res;
    }

    public static void main(String[] args) {
        List<List<Integer>> res = combine(4, 2);
        for (List<Integer> r : res){
            for (int a : r){
                System.out.print(a + " ");
            }
            System.out.println();
        }
    }
}
