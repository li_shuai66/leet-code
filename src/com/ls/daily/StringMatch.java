package com.ls.daily;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class StringMatch {
    public static List<String> stringMatching(String[] words) {
        Set<String> res = new HashSet<>();
        for (int i=0; i < words.length; i++) {
            for (int j=i+1; j < words.length; j++) {
                if (words[i].length() > words[j].length() && words[i].indexOf(words[j]) != -1) {
                    res.add(words[j]);
                } else if (words[i].length() < words[j].length() && words[j].indexOf(words[i])!=-1) {
                    res.add(words[i]);
                    break;
                }
            }
        }
        return new ArrayList<>(res);
    }

    public static void main(String[] args) {
        String[] a = {"leetcode","et","code"};
        stringMatching(a);
    }
}
