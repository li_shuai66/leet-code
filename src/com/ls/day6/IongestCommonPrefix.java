package com.ls.day6;

public class IongestCommonPrefix {

    public static void main(String[] args) {
        System.out.println();
    }

    public static String longestCommonPrefix(String[] strs) {
        if (strs.length==0 || strs[0].equals("")) return "";
        int i=0;
        while (i<strs[0].length()){
            try {
                for (int j=1;j<strs.length;j++){
                    if(strs[j].charAt(i) != strs[0].charAt(i)){
                        return strs[0].substring(0,i);
                    }
                }
            }catch (Exception e){
                return strs[0].substring(0,i);
            }
            i++;
        }
        return strs[0].substring(0,i);
    }
}
