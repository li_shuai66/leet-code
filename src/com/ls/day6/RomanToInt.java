package com.ls.day6;

public class RomanToInt {
    public static void main(String[] args) {
        System.out.println(romanToInt("II"));
    }
    public static int romanToInt(String s) {
        String[] res = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
        int[] w = {1000,900,500,400,100,90,50,40,10,9,5,4,1};
        int sum = 0;
        int i=0;
        for (int j=0;j<res.length;j++){
            if(s.length()-i>1 && s.substring(i,i+2).equals(res[j])){
                i+=2;
                sum+=w[j];
                j--;
            }

            if(i<s.length() && s.substring(i,i+1).equals(res[j])){
                i+=1;
                sum+=w[j];
                j--;
            }
        }

        return sum;
    }
}
