package com.ls.day6;

public class MaxArea {
    public static void main(String[] args) {

    }

    public int maxArea(int[] height) {
//        int max=0;
//        for (int i=0;i<height.length;i++){
//            for(int j=i+1;j<height.length;j++){
//                if ((j-i)*Math.min(height[i],height[j])>max){
//                    max=(j-i)*Math.min(height[i],height[j]);
//                }
//            }
//        }
        int max = 0,i=0,j=height.length-1;
        while (i<j){
            max = Math.max((j-i)*Math.min(height[i],height[j]),max);
            if(height[i]<height[j]){
                i++;
            }else {
                j--;
            }
        }
        return max;
    }
}
