package com.ls.day6;

public class IntToRoman {
    public static void main(String[] args) {
        System.out.println(intToRoman(1994));
    }

    public static String intToRoman(int num) {
        StringBuilder str = new StringBuilder();
        num = build(str,"M",1000,num);
        if (num > 0) num = build(str,"CM",900,num);
        if (num > 0) num = build(str,"D",500,num);
        if (num > 0) num = build(str,"CD",400,num);
        if (num > 0) num = build(str,"C",100,num);
        if (num > 0) num = build(str,"XC",90,num);
        if (num > 0) num = build(str,"L",50,num);
        if (num > 0) num = build(str,"XL",40,num);
        if (num > 0) num = build(str,"X",10,num);
        if (num > 0) num = build(str,"IX",9,num);
        if (num > 0) num = build(str,"V",5,num);
        if (num > 0) num = build(str,"IV",4,num);
        if (num > 0) num = build(str,"I",1,num);
        return str.toString();
    }

    public static int build(StringBuilder str,String s, int n,int num){
        int i=0;
        while (i<num/n){
            str.append(s);
            i++;
        }
        return num-num/n*n;
    }

}
