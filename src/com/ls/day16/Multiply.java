package com.ls.day16;

/**
 * @author ：xxx
 * @description：TODO
 * @date ：2021/4/1 11:16
 */
public class Multiply {

    public static void main(String[] args) {
        System.out.println(multiply("999","99"));
    }

    public static String multiply(String num1, String num2) {
        char[] s1 = num1.toCharArray();
        char[] s2 = num2.toCharArray();
        int[] res = new int[300];
        int i1=0,i2=0,j=0,b,d,tem;
        for (i1=s1.length-1;i1 >=0;i1--){
            d=0;b=0;
            for (i2=s2.length-1;i2>=0 || d!=0 && i2>=-1;i2--){
                if (i2>=0){
                    b=((s1[i1]-'0')*(s2[i2]-'0')+d)%10;
                    d = ((s1[i1]-'0')*(s2[i2]-'0')+d)/10;
                }else{
                    b=d;
                }
                int p = s1.length+s2.length-i1-i2-2;
                res[p] += b;

                tem = res[p]/10;
                res[p] = res[p]%10;
                j=1;
                while (tem!=0){
                    res[p+j] += tem;
                    tem = res[p+j]/10;
                    res[p+j] = res[p+j]%10;
                    j++;
                }
            }
        }
        j=299;
        while (res[j]==0){
            j--;
            if (j<0) return "0";
        }
        char[] result = new char[j+1];i1=0;
        while (j>=0){
            result[i1++]= (char) (res[j]+'0');
            j--;
        }
        return new String(result);
    }
}
