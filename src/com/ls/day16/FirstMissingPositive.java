package com.ls.day16;

/**
 * @author ：xxx
 * @description：TODO
 * @date ：2021/4/1 10:01
 */
public class FirstMissingPositive {
    public int firstMissingPositive(int[] nums) {
        int i=0;
        boolean[]  flag= new boolean[301];
        for (i=0;i<nums.length;i++){
            if (nums[i]>=1 && nums[i]<=300)flag[nums[i]]=true;
        }

        for (i=1;i<=300;i++){
            if (!flag[i])return i;
        }
        return 301;
    }
}
