package com.ls.day11;

public class RemoveDuCplicates {

    public static void main(String[] args) {
        int[] nums = {0,0,1,1,1,2,2,3,3,4};
        System.out.println(removeDuCplicates(nums));
        for (int n: nums){
            System.out.print(n+ " ");
        }
    }

    public static int removeDuCplicates(int[] nums) {
        if (nums.length==0) return 0;
        if (nums.length==1) return 1;
        int flag=0,count=0;
        for (int i=1;i<nums.length;){
            count++;
            while (i<nums.length&&nums[i]==nums[flag])i++;
            flag=i;
            if (i<nums.length)
                nums[count]=nums[i];
        }
        return count;
    }
}
