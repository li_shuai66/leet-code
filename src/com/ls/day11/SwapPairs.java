package com.ls.day11;

import com.ls.day2.ListNode;

public class SwapPairs {
    public ListNode swapPairs(ListNode head) {
        if(head==null || head.next==null) return head;
        ListNode h,p;
        h=head.next;
        head.next=h.next;
        h.next=head;
        if (h.next.next==null){
            return h;
        }
        p =  h.next;

        while (p.next!=null&&p.next.next!=null){
            swap(p);
            p=p.next.next;
        }
        return h;
    }

    public void swap(ListNode p){
        ListNode t = p.next.next;
        p.next.next=t.next;
        t.next=p.next;
        p.next=t;
    }
}
