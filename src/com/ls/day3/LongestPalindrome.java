package com.ls.day3;

public class LongestPalindrome {

    public static void main(String[] args) {
        System.out.println(longestPalindrome("bb"));
    }

    public static String longestPalindrome(String s) {
        int max=0,left=0,right=0,i=0,m=0,n=0;
        for(i=0;i<s.length();i++){
            //以这个字符为出发点，检测回文
            //第一种情况，以单个字符为中间点
            m=i-1;n=i+1;
            while (m>=0&&n<s.length()&&s.charAt(m)==s.charAt(n)){
                m--;n++;
            }
            if(max<(n-m-1)){
                max=n-m-1;
                left=m+1;
                right=n-1;
            }

            //第二种情况，以两个字符为出发点
            m=i;n=i+1;
            while (m>=0&&n<s.length()&&s.charAt(m)==s.charAt(n)){
                m--;n++;
            }
            if(max<(n-m-1)){
                max=n-m-1;
                left=m+1;
                right=n-1;
            }
        }
        return s.substring(left,right+1);
    }
}
