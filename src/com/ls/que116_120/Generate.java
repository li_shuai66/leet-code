package com.ls.que116_120;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：ls
 * @date ：2022/4/1 18:18
 * @description：...
 */
public class Generate {
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> res = new ArrayList<>();
        List<Integer> up, temp;
        temp = new ArrayList<>();
        temp.add(1);
        res.add(temp);
        up = new ArrayList<>();
        up.addAll(temp);
        int j = 2;
        for (int i=1; i<numRows; i++){
            temp = new ArrayList<>();
            temp.add(1);
            temp.add(1);
            for (int k=1; k<up.size(); k++){
                temp.add(1, up.get(k) + up.get(k-1));
            }
            res.add(temp);
            up.clear();
            up.addAll(temp);
        }
        return res;
    }
}
