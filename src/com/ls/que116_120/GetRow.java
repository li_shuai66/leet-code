package com.ls.que116_120;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：ls
 * @date ：2022/4/1 18:28
 * @description：给定一个非负索引 rowIndex，返回「杨辉三角」的第 rowIndex 行。
 *
 * 在「杨辉三角」中，每个数是它左上方和右上方的数的和。
 */
public class GetRow {
    public List<Integer> getRow(int rowIndex) {
        List<Integer> up, temp;
        temp = new ArrayList<>();
        temp.add(1);
        up = new ArrayList<>();
        up.addAll(temp);
        if (rowIndex == 1)
            return temp;
        for (int i=1; i<=rowIndex; i++){
            temp = new ArrayList<>();
            temp.add(1);
            temp.add(1);
            for (int k=1; k<up.size(); k++){
                temp.add(1, up.get(k) + up.get(k-1));
            }
            up.clear();
            up.addAll(temp);
        }
        return temp;
    }
}
