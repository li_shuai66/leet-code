package com.ls.day5;

public class IsToeplitzMatrix {
//    public static void main(String[] args) {
//        int[][] matrix = {{1,2,3,4},{5,1,2,3},{9,5,1,2}};
//        System.out.println(isToeplitzMatrix(matrix));
//    }
    public  boolean isToeplitzMatrix(int[][] matrix) {
        for (int i=0;i<matrix.length;i++){
            if (!isOk(matrix,i,0))
                return false;
        }

        for (int i=1;i<matrix[0].length;i++){
            if (!isOk(matrix,0,i))
                return false;
        }
        return true;
    }

    public  boolean isOk(int[][] matrix,int i,int j){
        int m=i+1,n=j+1;
        while (m<matrix.length&&n<matrix[m].length){
            if(matrix[m][n]!=matrix[i][j]) {
                return false;
            }
            m++;n++;
        }
        return true;
    }
}
