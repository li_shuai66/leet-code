package com.ls.day5;

public class MyAtoi {
    public static void main(String[] args) {
        System.out.println(myAtoi(""));
    }

    public static int myAtoi(String s) {
        if (s.length()==0)return 0;
        int i=0,flag=1;
        while (i< s.length()&&s.charAt(i)==' ') i++;
        if (i< s.length()&&s.charAt(i)=='+'){
            flag=1;
            i++;
        }else if(i< s.length()&&s.charAt(i)=='-'){
            flag=-1;
            i++;
        }

        int j=i;
        while (j<s.length() && s.charAt(j)>='0' && s.charAt(j)<='9')j++;
        if (j==i) return 0;
        try {
            return Integer.parseInt(s.substring(i,j))*flag;
        }catch (Exception e){
            if (flag==1)return Integer.MAX_VALUE;
            else return Integer.MIN_VALUE;
        }

    }
}
