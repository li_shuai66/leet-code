package com.ls.day5;

public class IsPalindrome {

    public static void main(String[] args) {

    }

    public static boolean isPalindrome(int x) {
        if (x<0) return false;
        else{
            String str = String.valueOf(x);
            for(int i=0;i<str.length()/2;i++){
                if(str.charAt(i)!=str.charAt(str.length()-i-1)){
                    return false;
                }
            }
        }
        return true;
    }
}
