package com.ls.day7;

import java.util.ArrayList;
import java.util.List;

public class Problem {
    public static void main(String[] args) {
        //第一种方法
        List<List<Integer>> items1 = new ArrayList<>();
        List<Integer> item1 = new ArrayList<>();
        for(int i=1;i<3;i++){
            item1.clear();
            item1.add(i);
            item1.add(i*2);
            item1.add(i*3);
            items1.add(item1);
        }

        System.out.println("第一种方法");
        show(items1);

        //第二种方法
        List<List<Integer>> items2 = new ArrayList<>();
        for(int i=1;i<3;i++){
            List<Integer> item2 = new ArrayList<>();
            item2.add(i);
            item2.add(i*2);
            item2.add(i*3);
            items2.add(item2);
        }

        System.out.println("第二种方法");
        show(items2);
    }

    public static void show(List<List<Integer>> items){
        for (List<Integer> item:items){
            for (Integer a:item){
                System.out.print(a+" ");
            }
            System.out.println();
        }
    }
}
