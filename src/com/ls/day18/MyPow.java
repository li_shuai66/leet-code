package com.ls.day18;

import java.util.Map;

/**
 * @author ：xxx
 * @description：TODO
 * @date ：2021/4/3 9:45
 */
public class MyPow {
    public double myPow(double x, int n) {
        return n>=0?quickmul(x,n):1/quickmul(x,-n);
    }

    public double quickmul(double x,int n){
        if (n==0) return 1.0;
        double a = quickmul(x,n/2);
        return n%2==0?a*a:a*a*x;
    }
}
