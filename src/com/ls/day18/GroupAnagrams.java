package com.ls.day18;

import java.util.*;

/**
 * @author ：xxx
 * @description：TODO
 * @date ：2021/4/3 9:14
 */
public class GroupAnagrams {
    public static void main(String[] args) {
        groupAnagrams(new String[]{""});
    }
    public static List<List<String>> groupAnagrams(String[] strs) {
        Map<String,List<String>>map = new HashMap<>();
        for (String s:strs){
            char[] c = s.toCharArray();
            Arrays.sort(c);
            String tem = String.valueOf(c);
            if (map.containsKey(tem)){
                map.get(tem).add(s);
            }else {
                List<String > n = new ArrayList<>();
                n.add(s);
                map.put(tem,n);
            }
        }
        List<List<String>> ans = new ArrayList<>();
        for (String key:map.keySet()){
            ans.add(map.get(key));
        }
        return ans;
    }
}
