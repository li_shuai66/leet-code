package com.ls.que106_110;

import com.ls.day2.ListNode;
import com.ls.utils.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：ls
 * @date ：2022/3/22 18:47
 * @description：...
 */
public class AortedListToBST {

    public TreeNode sortedListToBST(ListNode head) {
        List<Integer> nums = new ArrayList<>();
        while (head != null){
            nums.add(head.val);
            head = head.next;
        }
        return gen(nums, 0, nums.size()-1);
    }

    public TreeNode gen(List<Integer> nums, int l, int r){
        if (l > r)
            return null;

        int mid = (l + r) / 2;

        TreeNode node = new TreeNode(nums.get(mid));

        node.left = gen(nums, l , mid - 1);
        node.right = gen(nums, mid + 1, r);
        return node;
    }
}
