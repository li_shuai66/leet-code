package com.ls.que106_110;

import com.ls.utils.TreeNode;

/**
 * @author ：ls
 * @date ：2022/3/22 18:52
 * @description：...
 */
public class IsBalanced {
    boolean flag = true;
    public boolean isBalanced(TreeNode root) {
        deep(root);
        return flag;
    }

    public int deep(TreeNode node) {
        if (!flag)
            return -1;
        if (node == null)
            return 0;
        int left = deep(node.left);
        int right = deep(node.right);

        if (Math.abs(left-right) > 1)
            flag = false;

        return 1 + Math.max(left, right);
    }
}
