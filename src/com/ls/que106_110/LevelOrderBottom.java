package com.ls.que106_110;

import com.ls.utils.TreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author ：ls
 * @date ：2022/3/21 20:45
 * @description：...
 */
public class LevelOrderBottom {
    public List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        if (root==null)
            return res;
        List<TreeNode> now = new ArrayList<>();
        List<TreeNode> temp = new ArrayList<>();
        List<Integer> nums = new ArrayList<>();
        List<Integer> m = new ArrayList<>();
        m.add(root.val);
        res.add(m);
        now.add(root);
        while (now.size()!=0){
            temp.clear();
            nums.clear();
            for (TreeNode tr : now){
                if (tr.left!=null){
                    temp.add(tr.left);
                    nums.add(tr.left.val);
                }

                if (tr.right!=null){
                    temp.add(tr.right);
                    nums.add(tr.right.val);
                }
            }
            now.clear();
            now.addAll(temp);
            if (nums.size()!=0){
                List<Integer> t = new ArrayList<>();
                t.addAll(nums);
                res.add(t);
            }

        }
        Collections.reverse(res);
        return res;
    }
}
