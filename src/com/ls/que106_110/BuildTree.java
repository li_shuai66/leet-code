package com.ls.que106_110;

/**
 * @author ：ls
 * @date ：2022/3/21 20:38
 * @description：...
 */

import com.ls.utils.TreeNode;

/**
 * @author ：ls
 * @date ：2022/3/20 18:42
 * @description：...
 */
public class BuildTree {

    public TreeNode buildTree(int[] inorder, int[] postorder) {
        return gen(postorder, 0, postorder.length-1, inorder, 0, inorder.length-1);
    }

    public TreeNode gen(int[] postorder, int pl, int pr, int[] inorder, int il, int ir){
        if (pl > pr || il > ir)
            return null;

        // 找出当前节点
        TreeNode now = new TreeNode(postorder[pr]);

        // 在中序遍历中分割左右子树
        int k=0;
        for (int i=il; i<=ir;i++){
            if (inorder[i] == postorder[pr]){
                k = i;
                break;
            }
        }

        // 左子树的长度
        int len_l = k - il;

        // 进行下一次的循环
        // 左子树
        now.left = gen(postorder, pl, pl + len_l - 1, inorder, il, k-1);
        // 右子树
        now.right = gen(postorder, pl+len_l, pr-1, inorder, k+1, ir);
        return now;
    }
//    public static void main(String[] args) {
//        TreeNode treeNode = null;
//        gen(treeNode);
//        System.out.println(treeNode.val);
//    }
//
//    public static void gen(TreeNode node){
//        node = new TreeNode(1);
//    }
}
