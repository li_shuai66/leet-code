package com.ls.que106_110;

import com.ls.utils.TreeNode;

/**
 * @author ：ls
 * @date ：2022/3/21 20:47
 * @description：...
 */
public class SortedArrayToBST {
    public TreeNode sortedArrayToBST(int[] nums) {
        return gen(nums, 0, nums.length - 1);
    }

    public TreeNode gen(int[] nums, int l, int r){
        if (l > r)
            return null;

        int mid = (l + r) / 2;

        TreeNode node = new TreeNode(nums[mid]);

        node.left = gen(nums, l , mid - 1);
        node.right = gen(nums, mid + 1, r);
        return node;
    }
}
