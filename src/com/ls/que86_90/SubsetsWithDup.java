package com.ls.que86_90;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author ：ls
 * @date ：2022/3/10 12:13
 * @description：...
 */
public class SubsetsWithDup {
    public List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        Arrays.sort(nums);
        backTrace(nums, 0, res, new ArrayList<>());
        return res;
    }

    public void backTrace(int[] nums, int index, List<List<Integer>> res, List<Integer> tmp){
        List<Integer> t = new ArrayList<>();
        t.addAll(tmp);
        res.add(t);
        for (int i=index; i<nums.length;i++){
            if (i!=index && nums[i] == nums[i-1]){
                continue;
            }

            tmp.add(nums[i]);
            backTrace(nums, i+1, res, tmp);
            tmp.remove(tmp.size()-1);
        }
    }
}
