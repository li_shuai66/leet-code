package com.ls.que86_90;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author ：ls
 * @date ：2022/3/9 14:41
 * @description：...
 */
public class GrayCode {
    public List<Integer> grayCode(int n) {
        if (n==1) {
            List<Integer> list = new ArrayList<>();
            list.add(0);
            list.add(1);
            return list;
        }else {
            List<Integer> a, b;
            a = grayCode(n-1);
            b = new ArrayList<>();
            b.addAll(a);
            Collections.reverse(b);
            for (int val : b){
                a.add(val + (int)Math.pow(2, n-1));
            }
            return a;
        }
    }
}
