package com.ls.que86_90;

/**
 * @author ：ls
 * @date ：2022/3/8 10:06
 * @description：...
 */
public class Merge {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        if (m==0){
            for (int i=0;i<n;i++)
                nums1[i]=nums2[i];
            return;
        }
        if (n==0)
            return;
        int i=m-1, j=n-1, p = nums1.length - 1;
        while (i>=0 && j>=0){
            if (nums1[i]>nums2[j]){
                nums1[p] = nums1[i];
                i-=1;
            }else {
                nums1[p] = nums2[j];
                j-=1;
            }
            p-=1;
        }
        while (i>=0){
            nums1[p] = nums1[i];
            p-=1;
            i-=1;
        }
        while (j>=0){
            nums1[p] = nums2[j];
            p-=1;
            j-=1;
        }
    }
}
