package com.ls.que86_90;

import com.ls.day2.ListNode;

/**
 * @author ：ls
 * @date ：2022/3/7 8:36
 * @description：...
 */
public class Partition {
    public static ListNode partition(ListNode head, int x) {
        if (head==null) return null;
        ListNode p, pre, q=head, init = new ListNode(-201);
        init.next = head;
        p = init;
        pre = init;
        head = init;
        while (q!=null){
            if (q.val < x){
                pre.next = q.next;
                q.next = p.next;
                p.next = q;
                if (pre == p){
                    pre = p.next;
                }
                p = q;
                q = pre.next;

            }else {
                pre = q;
                q = q.next;
            }
        }
        return head.next;
    }

    public static void main(String[] args) {
        ListNode n1 = new ListNode(1);
        ListNode n2 = new ListNode(4);
        ListNode n3 = new ListNode(3);
        ListNode n4 = new ListNode(2);
        ListNode n5 = new ListNode(5);
        ListNode n6 = new ListNode(2);
        n1.next = n2;
        n2.next = n3;
        n3.next = n4;
        n4.next = n5;
        n5.next = n6;
        n6.next = null;
        partition(n1, 3);
    }
}
