package com.ls.day17;

/**
 * @author ：xxx
 * @description：TODO
 * @date ：2021/4/2 11:19
 */
public class Rotate {
    public static void main(String[] args) {
        rotate(new int[][]{{5,1,9,11},{2,4,8,10},{13,3,6,7},{15,14,12,16}});
    }

    public static void rotate(int[][] matrix) {
        int i,j,k,t,c;
        for (k=0;k<matrix.length/2;k++){        //有多少层
            i=k-1;
            for (t=0;t<matrix.length-1-2*k;t++){    //每层处理几次
                i++;j=k;c=3;
                int lasti=i,lastj=j,tem=0,r=matrix[i][j];
                int p=i;
                while (c>0){        //处理每一次，总共需要三次
                    tem=i;
                    i=matrix.length-1-j;
                    j=tem;
                    matrix[lasti][lastj] = matrix[i][j];
                    lasti=i;lastj=j;
                    c--;
                }
                matrix[i][j]=r;       //处理最后一次
                i=p;
            }
        }
    }

}
