package com.ls.day17;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：xxx
 * @description：TODO
 * @date ：2021/4/2 10:29
 */
public class Permute {
    List<List<Integer>> ans = new ArrayList<>();
    List<Integer> tem = new ArrayList<>();
    public List<List<Integer>> permute(int[] nums) {
        boolean[] visit = new boolean[nums.length];
        def(nums,visit);
        return ans;
    }

    public void def(int[] nums,boolean[] visit){
        if (tem.size()==nums.length){
            List<Integer> a = new ArrayList<>(tem);
            ans.add(a);
            return;
        }
        for (int i=0;i<nums.length;i++){
            if (!visit[i]){
                visit[i]=true;
                tem.add(nums[i]);
                def(nums,visit);
                visit[i]=false;
                tem.remove(tem.size()-1);
            }
        }
    }
}
