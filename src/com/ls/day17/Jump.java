package com.ls.day17;

/**
 * @author ：xxx
 * @description：TODO
 * @date ：2021/4/2 9:56
 */
public class Jump {

    public static void main(String[] args) {
        System.out.println(jump(new int[]{2,3,0,1,4}));
    }

    public static int jump(int[] nums) {
        int[] count = new int[nums.length];
        int i=0,j=0;
        for (i=nums.length-2;i>=0;i--){
            int min=Integer.MAX_VALUE;
            for (j=i+1;j<=i+nums[i] && j<nums.length;j++){
                if (count[j]<min)min=count[j];
            }
            if (nums[i]==0 || min == Integer.MAX_VALUE)count[i] = Integer.MAX_VALUE;
            else {
                count[i] = min+1;
            }
        }
        return count[0];
    }
}
