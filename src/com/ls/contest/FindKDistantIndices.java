package com.ls.contest;

import java.util.*;

/**
 * @author ：ls
 * @date ：2022/3/13 11:41
 * @description：...
 */
public class FindKDistantIndices {
    public static List<Integer> findKDistantIndices(int[] nums, int key, int k) {
        Set<Integer> res = new HashSet<>();
        int i=0,j=0;
        for(i=0;i<nums.length;i++){
            if (nums[i] == key){
                for (j= Math.max(0, i-k); j<=i+k && j<nums.length; j++){
                    res.add(j);
                }
            }
        }
        List<Integer> r = new ArrayList<>(res);
        Collections.sort(r);
        return r;
    }

    public static void main(String[] args) {
        List<Integer> res = findKDistantIndices(new int[]{2,2,2,2,2}, 2, 2);
        for (int r : res)
            System.out.print(r + " ");
    }
}
