package com.ls.que101_105;

import com.ls.utils.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：ls
 * @date ：2022/3/17 14:38
 * @description：...
 */
public class IsSymmetric {
    public boolean isSymmetric(TreeNode root) {
        List<TreeNode> now = new ArrayList<>();
        now.add(root);
        List<TreeNode> temp = new ArrayList<>();
        TreeNode tr = new TreeNode(-101);

        while (now.size()!=0){
            temp.clear();
            for (TreeNode node : now){
                if (node == tr)
                    continue;
                if (node.left!=null)
                    temp.add(node.left);
                else {
                    temp.add(tr);
                }
                if (node.right!=null)
                    temp.add(node.right);
                else {
                    temp.add(tr);
                }
            }

            // 检查部分
            int i=0, j=temp.size()-1;
            while (i<j){
                if (temp.get(i).val != temp.get(j).val)
                    return false;
                i++;
                j--;
            }
            now.clear();
            now.addAll(temp);
        }
        return true;
    }



}
