package com.ls.que101_105;

import com.ls.utils.TreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author ：ls
 * @date ：2022/3/19 9:47
 * @description：...
 */
public class ZigzagLevelOrder {
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        if (root==null)
            return res;
        List<TreeNode> now = new ArrayList<>();
        List<TreeNode> temp = new ArrayList<>();
        List<Integer> nums = new ArrayList<>();
        List<Integer> m = new ArrayList<>();
        m.add(root.val);
        res.add(m);
        now.add(root);
        int i =1;
        while (now.size()!=0){
            temp.clear();
            nums.clear();
            for (TreeNode tr : now){
                if (tr.left!=null){
                    temp.add(tr.left);
                    nums.add(tr.left.val);
                }

                if (tr.right!=null){
                    temp.add(tr.right);
                    nums.add(tr.right.val);
                }
            }
            now.clear();
            now.addAll(temp);
            if (nums.size()!=0){
                List<Integer> t = new ArrayList<>();
                t.addAll(nums);
                if (i%2 == 1)
                    Collections.reverse(t);
                res.add(t);
                i+=1;
            }

        }
        return res;
    }
}
