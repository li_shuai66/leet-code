package com.ls.que101_105;

import com.ls.utils.TreeNode;

/**
 * @author ：ls
 * @date ：2022/3/19 10:04
 * @description：...
 */
public class MaxDepth {
    int max_depth=0;
    public int maxDepth(TreeNode root) {
        if (root==null)
            return 0;
        travel(root, 1);
        return max_depth;
    }

    public void travel(TreeNode node, int depth) {
        max_depth = Math.max(depth, max_depth);
        if (node.left !=null)
            travel(node.left, depth+1);
        if (node.right!=null)
            travel(node.right, depth+1);
    }
}
