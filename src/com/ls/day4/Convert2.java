package com.ls.day4;

public class Convert2 {
    public static void main(String[] args) {
        System.out.println(convert("AB",1));
    }

    public static String convert(String s, int numRows) {
        if (numRows==1)return s;
        String[] rows = new String[Math.min(numRows,s.length())];
        for(int i=0;i<Math.min(numRows,s.length());i++){
            rows[i]="";
        }
        //等于0或者numRows-1时发生转向
        boolean flag = false;
        int i = 0,curRow=0;
        while (i<s.length()){
            if(curRow==0 || curRow == numRows-1){
                flag = !flag;
            }
            rows[curRow]+=String.valueOf(s.charAt(i));
            curRow+=flag?1:-1;
            i++;
        }
        String result = "";
        for (String str:rows){

            result+=str;
        }


        return result;
    }
}
