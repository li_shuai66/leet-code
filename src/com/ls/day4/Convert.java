package com.ls.day4;

public class Convert {
    public static void main(String[] args) {
        System.out.println(convert("",2));
    }

    public static String convert(String s, int numRows) {
        if (numRows==1)return s;
        int rows=numRows,cols=0,i=0,j=0,times=0;
        cols = s.length()/(2*numRows-2)*(numRows-1);
        if(s.length()-s.length()/(2*numRows-2)*(2*numRows-2)>0)
            cols++;
        if(((s.length()-(s.length()/(2*numRows-2))*(2*numRows-2))-numRows)>0){
            cols += (s.length()-(s.length()/(2*numRows-2))*(2*numRows-2))-numRows;
        }
//        System.out.println(rows + " " + cols);
        char term[][] = new char[numRows][cols];
        //times用于计数
        for (times=0;times<s.length();){
            //每次分为两部分进行计算
            while (rows>0){
                if (times==s.length()) break;
                term[i][j]=s.charAt(times);
                i++;
                times++;
                rows--;
            }
            i--;
            rows = numRows;


            int p = numRows-2;
            while (p>0){
                if (times==s.length()) break;
                i--;j++;
                term[i][j]=s.charAt(times);
                times++;
                p--;
            }
            i--;j++;
        }

        //查看一下数据
//        for(int m=0;m<numRows;m++){
//            for(int n=0;n<cols;n++){
//                if (term[m][n]<'A'||term[m][n]>'Z'){
//                    System.out.print(" ");
//                }else{
//                    System.out.print(term[m][n]);
//                }
//            }
//            System.out.println("");
//        }

        //收集数据
        String result = "";
        for(int m=0;m<numRows;m++){
            for(int n=0;n<cols;n++){
                if(term[m][n]>='A'&&term[m][n]<='Z' ||term[m][n]>='a'&&term[m][n]<='z' || term[m][n]==',' || term[m][n]=='.'){
                    result+=String.valueOf(term[m][n]);
                }
            }
        }
        return result;
    }
}
