package com.ls.day4;

public class Reverse2 {
    public static int reverse(int x) {
        long l = 0;
        while (x!=0){
            l = l*10 + x%10;
            x/=10;
        }
        return (int)l == l ? (int)l:x;
    }
}
