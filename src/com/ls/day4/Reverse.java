package com.ls.day4;

public class Reverse {
    public static void main(String[] args) {
        System.out.println(reverse(-123));
    }

    public static int reverse(int x) {
        if(x==0)return 0;
        char[] s = String.valueOf(x).toCharArray();
        char p;
        //反转
        if(x>0){
            for(int i=0;i<s.length/2;i++){
                p = s[i];
                s[i]=s[s.length-i-1];
                s[s.length-i-1]=p;
            }
        }else{
            for(int i=0;i<(s.length-1)/2;i++){
                p = s[i+1];
                s[i+1]=s[s.length-i-1];
                s[s.length-i-1]=p;
            }
        }

        double q = Double.valueOf(String.valueOf(s));
        if(q>2.147483648E9-1 || q< -2.147483648E9)
            return 0;
        return (int)q;
    }
}
