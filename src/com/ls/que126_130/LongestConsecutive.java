package com.ls.que126_130;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;


/**
 * @author ：ls
 * @date ：2022/4/11 18:27
 * @description：...
 */
public class LongestConsecutive {

  public int longestConsecutive(int[] nums) {
    Map<Integer, Integer> util = new TreeMap<>();
    int max_val = -1;
    for (int i=0; i<nums.length; i++) {

      
    }
    for (int i = 0; i < nums.length; i++) {
      util.put(nums[i], util.get(nums[i]-1) == null ? 1 : util.get(nums[i]-1) + 1);
      max_val = Math.max(max_val, util.get(nums[i]));
    }
    return max_val;
  }

  public static void main(String[] args) {
    System.out.println("hello world");
    Map<Integer, Integer> util = new HashMap<>();
    util.put(1, 1);
    util.put(2, 2);
    util.put(3, 3);
    System.out.println(util.get(2));
    System.out.println(util.get(4));
  }
}
