package com.ls.day15;

/**
 * @author ：xxx
 * @description：TODO
 * @date ：2021/3/31 9:40
 */
public class CountAndSay {
    public String countAndSay(int n) {
        if (n==1) return "1";
        String s_front = countAndSay(n-1);
        StringBuffer sb = new StringBuffer();
        int i,count=0,flag=0;
        for (i=0;i< s_front.length();i++){
            if (s_front.charAt(i)==s_front.charAt(flag)){
                count++;
            }else {
                sb.append(count+""+s_front.charAt(flag));
                flag=i;
                count=0;
                i--;
            }
        }
        sb.append(count+""+s_front.charAt(flag));
        return sb.toString();
    }
}
