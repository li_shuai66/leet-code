package com.ls.day10;

public class Queen {

    public static void main(String[] args) {
        int[] array = new int[8];
        System.out.println(queen(array));
    }

    public static int queen(int[] array){
        int n = array.length-1;
        int k=0,count=0;
        while (k>=0){
            while (array[k]<=n && !check(array,k)){
                array[k]++;
            }
            if (array[k]>n){
                k--;
                if (k<0)break;
                array[k]++;
            }else {
                if (k<n){
                    k++;
                    array[k]=0;
                }else{
                    count++;
                    k--;
                    array[k]++;
                }
            }
        }
        return count;
    }

     static boolean check(int[] array, int n){
        for (int i=0;i<n;i++){
            if (Math.abs(array[n]-array[i])==Math.abs(n-i) || array[n]==array[i])
                return false;
        }
        return true;
    }
}
