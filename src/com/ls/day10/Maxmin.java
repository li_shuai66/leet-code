package com.ls.day10;

public class Maxmin {

    public static void main(String[] args) {
        int[] array = {1,5,67,34,2,45,2,1};
        quicksort(array,0,array.length-1);
        show(array);
    }

    public static int getmin(int[] array, int l, int r){
        if (l==r)return array[l];
        return Math.min(getmin(array,l,(l+r)/2),getmin(array,(l+r)/2+1,r));
    }

    public static void quicksort(int[] array,int l,int r){
        if (l>=r)return;
        int mid = sort(array,l,r);
        quicksort(array,l,mid-1);
        quicksort(array,mid+1,r);
    }

    public static int sort(int[] array,int l,int r){
        int temp = array[l];
        int i=l,j=r;
        while (i<j){
            while (i<j&&array[j]>=temp)j--;
            if (i<j){
                array[i]=array[j];
                i++;
            }
            while (i<j&&array[i]<=temp)i++;
            if (i<j){
                array[j]=array[i];
                j--;
            }

        }
        array[i]=temp;
        return i;
    }

    public static void show(int[] array){
        for(int i=0;i<array.length;i++){
            System.out.print(array[i]+" ");
        }
    }
}
