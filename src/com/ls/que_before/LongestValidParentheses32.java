package com.ls.que_before;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @author ：ls
 * @date ：2021/12/3 10:39
 * @description：...
 */
public class LongestValidParentheses32 {
    public static int longestValidParentheses(String s) {
        if (s.length() == 0)
            return 0;
        List<Character> stack = new ArrayList<>();
        List<Integer> place = new ArrayList<>();
        int max = 0;
        char[] ss = s.toCharArray();
        for (int i=0; i< ss.length; i++){
            if (ss[i] == '('){
                stack.add('(');
                place.add(i);
            }else {
                if (stack.size() == 0 || stack.get(stack.size()-1) == ')'){
                    continue;
                }
                if (stack.get(stack.size()-1) == '('){
                    ss[i] = 'a';
                    ss[place.get(place.size()-1)]='a';
                    stack.remove(stack.size()-1);
                    place.remove(place.size()-1);
                }
            }
        }

        int temp = 0;
        for (int i=0; i<ss.length;i++){
            if (ss[i] == 'a'){
                temp += 1;
                if (temp > max)
                    max = temp;
            }else {
                temp =0;
            }
        }
        return max;
    }

    public static void main(String[] args) {
        System.out.println(longestValidParentheses(")()())"));
    }
}
