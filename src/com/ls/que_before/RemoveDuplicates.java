package com.ls.que_before;

/**
 * @author ：ls
 * @date ：2021/12/1 11:14
 * @description：...
 */
public class RemoveDuplicates {
    public int removeDuplicates(int[] nums) {
        if (nums.length == 0 || nums.length == 1)
            return nums.length;
        int i=0, j=1;
        for (j = 1; j < nums.length;j++){
            if (nums[j] != nums[i]){
                i ++;
                nums[i] = nums[j];
            }
        }
        return i+1;
    }
}
