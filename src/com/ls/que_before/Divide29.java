package com.ls.que_before;

/**
 * @author ：ls
 * @date ：2021/12/3 10:12
 * @description：...
 */
public class Divide29 {
    public int divide(int dividend, int divisor) {
        if (dividend == Integer.MIN_VALUE && divisor == -1)
            return Integer.MAX_VALUE;
        if (dividend < divisor)
            return 0;
        if (dividend == divisor)
            return 1;

        int times = 1;
        long temp = divisor;
        while (dividend > temp){
            temp += temp;
            times += times;
        }
        return 0;
    }
}
