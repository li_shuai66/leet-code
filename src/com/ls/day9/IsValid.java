package com.ls.day9;

public class IsValid {
    public boolean isValid(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i=0;i<s.length();i++){
            if (s.charAt(i)=='(' || s.charAt(i)=='[' || s.charAt(i)=='{'){
                sb.append(s.charAt(i));
            }else if (s.charAt(i)==')'){
                if (sb.length()>0&&sb.charAt(sb.length()-1)=='('){
                    sb.deleteCharAt(sb.length()-1);
                }else {
                    return false;
                }
            }else if (s.charAt(i)==']'){
                if (sb.length()>0&&sb.charAt(sb.length()-1)=='['){
                    sb.deleteCharAt(sb.length()-1);
                }else {
                    return false;
                }
            }else {
                if (sb.length()>0&&sb.charAt(sb.length()-1)=='{'){
                    sb.deleteCharAt(sb.length()-1);
                }else {
                    return false;
                }
            }
        }

        if (sb.length()==0)return true;
        return false;
    }
}
