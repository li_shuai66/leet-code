package com.ls.day9;

import java.util.List;

public class RemoveNthFromEnd {
    public static ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode t = head;
        ListNode h =head;
        while (n>0){
            t=t.next;
            n--;
        }

        if(t==null) return head.next;
        while (t.next!=null){
            t=t.next;
            h=h.next;
        }

        if (head.next==null)return null;
        else{
            h.next=h.next.next;
            return head;
        }
    }
}
