package com.ls.day9;

public class MergeTwoLists {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1==null && l2==null)return null;
        if (l1==null && l2!=null) return l2;
        if (l1!=null && l2==null) return l1;
        ListNode p=l1,q=l2,f,h;
        if (p.val<q.val){
            f=p;p=p.next;
            h=p;
        }else {
            f=q;q=q.next;
            h=q;
        }
        while (p!=null&&q!=null){
            if (p.val<q.val){
                f.next=p;
                f=p;
                p=p.next;
            }
            else{
                f.next=q;
                f=q;
                q=q.next;

            }
        }

        if (p==null)f.next=q;
        if (q==null)f.next=p;
        return h;
    }
}
