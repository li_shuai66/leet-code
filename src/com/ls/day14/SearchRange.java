package com.ls.day14;

/**
 * @author ：xxx
 * @description：TODO
 * @date ：2021/3/30 22:22
 */
public class SearchRange {
    public static void main(String[] args) {
        int nums[] = new int[]{};
        int res[] = searchRange(nums,0);
        for(int a:res){
            System.out.print(a + " ");
        }
    }
    public static int[] searchRange(int[] nums, int target) {
        // 二分法
        int l=0,r=nums.length-1,mid=0;
        while (l<=r){
            mid = (l+r)/2;
            if (nums[mid]<target) l=mid+1;
            else if (nums[mid]==target)break;
            else r=mid-1;
        }
        if (l>r)return new int[]{-1,-1};
        l=mid;r=mid;
        while (l>=0 && nums[l]==nums[mid])l--;
        while (r<=nums.length-1 && nums[r]==nums[mid])r++;
        l++;r--;
        return new int[]{l,r};
    }
}
