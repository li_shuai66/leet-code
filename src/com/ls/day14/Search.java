package com.ls.day14;

/**
 * @author ：xxx
 * @description：TODO
 * @date ：2021/3/30 21:31
 */
public class Search {

    public static void main(String[] args) {
        int nums[] = {1,3,5};
        System.out.println(search(nums,3));
    }

    public static int search(int[] nums, int target) {
        int l=0,r=nums.length-1,mid;
        while (l<r){
            mid = (l+r)/2;
            if (nums[l]<nums[mid]){   //左边递增
                if (target>=nums[l] && target <=nums[mid]){
                    r=mid;
                    break;
                }else{
                    l=mid+1;
                }
            }else {
                if (target>=nums[mid+1] && target <=nums[r]){
                    l=mid+1;
                    break;
                }else{
                    r=mid;
                }
            }
        }
        if (l==r){
            if (nums[l]==target)return l;
            else return -1;
        }

        // 二分查找
        while (l<=r){
            mid = (l+r)/2;
            if (nums[mid]<target) l=mid+1;
            else if (nums[mid]==target) return mid;
            else r=mid-1;
        }
        return -1;
    }


}
