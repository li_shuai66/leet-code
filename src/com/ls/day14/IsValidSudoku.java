package com.ls.day14;

/**
 * @author ：xxx
 * @description：TODO
 * @date ：2021/3/30 22:45
 */
public class IsValidSudoku {
    public boolean isValidSudoku(char[][] board) {
        int i,j,k;
        boolean[] flag1 = new boolean[9];
        boolean[] flag2 = new boolean[9];
        for (i=0;i<9;i++){
            flag1[i]=false;
            flag2[i]=false;
        }
        //检测行、列
        for (i=0;i<9;i++){
            for (k=0;k<9;k++){
                flag1[k]=false;
                flag2[k]=false;
            }
            for (j=0;j<9;j++){
                if (board[i][j]!='.'){
                    int p = board[i][j]-'0'-1;
                    if (flag1[p])return false;
                    flag1[p]=true;
                }

                if (board[j][i]!='.'){
                    int p = board[j][i]-'0'-1;
                    if (flag2[p])return false;
                    flag2[p]=true;
                }
            }
        }

        //检测九宫格
        for (i=0;i<3;i++){
            for (j=0;j<3;j++){
                if (!check(board,i*3,j*3)){
                    return false;
                }
            }
        }
        return true;
    }

    public boolean check(char[][] board,int k,int t){
        //检测九宫格
        boolean[] flag = new boolean[9];
        int i,j;
        for (i=0;i<9;i++)flag[i]=false;
        for (i=k;i<k+3;i++){
            for (j=t;j<t+3;j++){
                if (board[i][j]!='.'){
                    int p = board[i][j]-'0'-1;
                    if (flag[p])return false;
                    flag[p]=true;
                }
            }
        }
        return true;
    }
}
