package com.ls.que111_115;

import com.ls.utils.TreeNode;

/**
 * @author ：ls
 * @date ：2022/3/23 18:25
 * @description：...
 */
public class HasPathSum {
    boolean flag = false;
    public boolean hasPathSum(TreeNode root, int targetSum) {
        if (root == null)
            return false;
        travel(root, 0, targetSum);
        return flag;
    }

    public void travel(TreeNode node, int sum, int targetSum) {
        if (flag)
            return;

        sum += node.val;
        if (node.left == null && node.right == null && sum == targetSum){
            flag = true;
            return;
        }

        if (node.left!=null)
            travel(node.left, sum, targetSum);

        if (node.right!=null)
            travel(node.right, sum, targetSum);
    }
}
