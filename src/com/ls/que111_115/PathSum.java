package com.ls.que111_115;

import com.ls.utils.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：ls
 * @date ：2022/3/23 18:31
 * @description：...
 */
public class PathSum {
    public List<List<Integer>> pathSum(TreeNode root, int targetSum) {
        List<List<Integer>> res = new ArrayList<>();
        if (root == null)
            return res;
        travel(root, 0, targetSum, new ArrayList<Integer>(), res);
        return res;
    }

    public void travel(TreeNode node, int sum, int targetSum, List<Integer> temp, List<List<Integer>> res) {
        sum += node.val;
        temp.add(node.val);
        if (node.left == null && node.right == null && sum == targetSum){
            List<Integer> t = new ArrayList<>();
            t.addAll(temp);
            res.add(t);
            temp.remove(temp.size()-1);
            return;
        }

        if (node.left!=null)
            travel(node.left, sum, targetSum, temp, res);

        if (node.right!=null)
            travel(node.right, sum, targetSum, temp, res);
        temp.remove(temp.size()-1);
    }
}
