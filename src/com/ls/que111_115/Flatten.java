package com.ls.que111_115;

import com.ls.utils.TreeNode;

/**
 * @author ：ls
 * @date ：2022/3/24 19:40
 * @description：...
 */
public class Flatten {
    TreeNode parent = null;
    public void flatten(TreeNode root) {
        if (root == null)
            return;
        parent = root;
        TreeNode temp = root.right;
        if (root.left!=null)
            travel(root.left);
        if (temp!=null)
            travel(temp);

    }

    public void travel(TreeNode node) {
        parent.left = null;
        parent.right = node;
        parent = node;
        TreeNode temp = node.right;
        if (node.left!=null)
            travel(node.left);
        if (temp!=null)
            travel(temp);

    }
}
