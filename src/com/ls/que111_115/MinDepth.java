package com.ls.que111_115;


import com.ls.utils.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：ls
 * @date ：2022/3/22 19:02
 * @description：...
 */
public class MinDepth {
    public int minDepth(TreeNode root) {
        if (root == null)
            return 0;

        List<TreeNode> now = new ArrayList<>();
        List<TreeNode> temp = new ArrayList<>();

        now.add(root);
        int deep = 1;
        while (now.size() > 0 ){
            for (TreeNode tr : now){
                if (tr.left == null && tr.right==null)
                    return deep;
                if (tr.left!=null)
                    temp.add(tr.left);
                if (tr.right!=null)
                    temp.add(tr.right);
            }
            now.clear();
            now.addAll(temp);
            temp.clear();
            deep += 1;
        }
        return deep;
    }
}
