package com.ls.que68_70;

/**
 * @author ：ls
 * @date ：2021/11/26 20:50
 * @description：...
 */
public class MySqrt {
    public static int mySqrt(int x) {
        // 步长设置为1000
        long step = 1000l;
        long val = 0l;
        int i = 4;
        while (i-- > 0){
            while (val * val <= x)
                val += step;

            val -= step;
            step /= 10;
        }

        return (int)val;
    }

    public static void main(String[] args) {
        System.out.println(mySqrt(8));
    }
}
