package com.ls.que68_70;

/**
 * @author ：ls
 * @date ：2021/11/27 20:20
 * @description：...
 */
public class ClimbStairs {

    public static int climbStairs(int n) {
        if (n==1)
            return 1;
        if (n==2)
            return 2;
        int a=1,b=2,i=0;
        while (i++<(n+1)/2-1){
            a = a + b;
            b = a + b;
        }
        if (n%2==1)
            return a;
        else
            return b;
    }

    public static void main(String[] args) {
        System.out.println(climbStairs(4));
    }
}
