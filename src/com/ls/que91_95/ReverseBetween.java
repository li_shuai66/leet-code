package com.ls.que91_95;

import com.ls.day2.ListNode;

import java.util.List;

/**
 * @author ：ls
 * @date ：2022/3/10 14:00
 * @description：...
 */
public class ReverseBetween {
    public ListNode reverseBetween(ListNode head, int left, int right) {
        if (left == right)
            return head;
        ListNode p, q=new ListNode(), r, t;
        q.next = head;
        head = q;
        p = q;
        int cnt = left;
        while (cnt > 1){
            cnt-=1;
            p = p.next;
        }
        q = p.next;
        t = q;
        p.next = null;
        cnt = right - left + 1;
        while (q!=null && cnt > 0){
            cnt -= 1;
            r = q.next;
            q.next = p.next;
            p.next = q;
            q = r;
        }
        t.next = q;
        return head.next;
    }
}
