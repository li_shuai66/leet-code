package com.ls.que91_95;

import com.ls.utils.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：ls
 * @date ：2022/3/11 10:55
 * @description：...
 */
public class InorderTraversal {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        if (root==null)
            return res;
        traversal(root, res);
        return res;
    }

    public void traversal(TreeNode treeNode, List<Integer> res){
        if (treeNode.left!=null)
            traversal(treeNode.left, res);
        res.add(treeNode.val);
        if (treeNode.right!=null)
            traversal(treeNode.right,res);
    }
}
