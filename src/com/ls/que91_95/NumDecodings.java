package com.ls.que91_95;

/**
 * @author ：ls
 * @date ：2022/3/10 12:35
 * @description：...
 */
public class NumDecodings {
    public static int numDecodings(String s) {
        if (s.charAt(0) == '0')
            return 0;
        char[] ss = s.toCharArray();
        int a=1,b=1,c=1, t;
        for (int i=1; i<ss.length;i++){
            c=0;
            if (ss[i] == '0'){
                if (ss[i-1] != '1' && ss[i-1] != '2')
                    return 0;
                c=a;
                b=a;
                continue;
            }
            c+=b;
            t = Integer.parseInt(s.substring(i-1,i+1));
            if (t>=10 && t<=26){
                c += a;
                a=b;
                b=c;
            }else {
                c=b;
                a=b;
            }
        }
        return c;
    }

    public static void main(String[] args) {
        System.out.println(numDecodings("123123"));
    }
}
