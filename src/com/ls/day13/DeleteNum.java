package com.ls.day13;

public class DeleteNum {

    public static void main(String[] args) {
//        char[] num = {'2','3','1','1','8','3'};
//        char[] num = {'1','2','3','4','5','6','7'};
        char[] num = {'1','2','0','0','8','3'};
        int s =3;
        System.out.println(delete(num,s));
    }

    public static int delete(char[] num,int s){
        for (int i = 0; i < num.length-1;i++){
            if (!(num[i]>='0' && num[i]<='9')) continue;
            else {
                if(!(num[i+1]>='0' && num[i+1]<='9')){
                    if (num[i]>num[num[i+1]-'a'-'0']){
                        s--;
                        num[i]=num[i+1];
                        i=-1;
                    }
                }else{
                    if (num[i]>num[i+1]){
                        s--;
                        num[i]= (char) ('0'+'a'+i+1);
                        i=-1;
                    }
                }
            }
        }
        char[] res = new char[num.length];
        int j=0;
        for (int i=0;i<num.length;i++){
            if (num[i]>='0' && num[i]<='9'){
                res[j++]=num[i];
            }
        }

        String ss = new String(res);
        ss = ss.trim();
        return Integer.parseInt(ss.substring(0,ss.length()-s));
    }
}
