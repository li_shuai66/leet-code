package com.ls.day13;

public class CountTowers {

    public static void main(String[] args) {
        int[][] num = {{9},{12,15},{10,6,8},{2,18,9,5},{19,7,10,4,16}};
        count(num);
        for (int i=0;i<num.length;i++){
            for (int j=0;j<=i;j++)
                System.out.print(num[i][j]+ " ");
            System.out.println("");
        }
    }

    public static void count(int[][] num){
        for (int i=num.length-2;i>=0;i--){
            for (int j=0;j<=i;j++){
                num[i][j] = Math.max(num[i+1][j],num[i+1][j+1])+num[i][j];
            }
        }
    }
}
