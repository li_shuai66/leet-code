package com.ls.day13;

public class LongestValidParentheses {
    public int longestValidParentheses(String s) {
        if (s.length()==0) return 0;
        char[] str = s.toCharArray();
        char[] sta = new char[str.length]; int p=0;
        int count=0;
        for (int i=0;i<str.length;i++){
            if (str[i]=='('){
                sta[p++]=str[i];
            }else{
                if (p>0&&sta[p-1]=='('){
                    count++;
                    p--;
                }
            }
        }
        return count*2;
    }
}
