package com.ls.day12;

import java.util.ArrayList;
import java.util.Arrays;

public class NextPermutation {

    public static void main(String[] args) {
        int[] nums = {1,1,5};
        nextPermutation(nums);
        for (int n:nums){
            System.out.print(n+" ");
        }
    }

    public static void nextPermutation(int[] nums) {
        int flag=-1,i=0,j=0,n=0;
        for (i=nums.length-1;i>=0;i--){
            for (j=i-1;j>flag&&j>=0;j--){
                if (nums[j]<nums[i]){
                    flag=j;
                    n=i;
                }
            }
        }

        if (flag==-1)Arrays.sort(nums);
        else {
            int tem = nums[flag];
            nums[flag]=nums[n];
            nums[n]=tem;
            Arrays.sort(nums,flag+1,nums.length);
        }
    }

}
