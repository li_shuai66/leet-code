package com.ls.day12;

public class StrStr {
    public int strStr(String haystack, String needle) {
        if (needle.length()==0)return 0;
        int len = needle.length();
        for (int i=0;i<haystack.length()-len+1;i++){
            if (haystack.substring(i,i+len).equals(needle))return i;
        }
        return -1;
    }
}
