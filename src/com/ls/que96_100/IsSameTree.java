package com.ls.que96_100;

import com.ls.utils.TreeNode;

/**
 * @author ：ls
 * @date ：2022/3/17 13:37
 * @description：...
 */
public class IsSameTree {
    public static boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q==null)
            return true;
        if (p!=null && q==null || p==null && q!=null)
            return false;
        return travel(p, q);
    }

    public static boolean travel(TreeNode p, TreeNode q){
        if (p.val != q.val)
            return false;
        boolean flag1, flag2;
        if (p.left != null && q.left == null || p.left == null && q.left !=null) {
            return false;
        }else {
            if (p.left == null){
                flag1 = true;
            }else {
                flag1 = travel(p.left, q.left);
            }
        }

        if (p.right != null && q.right == null || p.right == null && q.right !=null) {
            return false;
        }else {
            if (p.right == null){
                flag2 = true;
            }else {
                flag2 = travel(p.right, q.right);
            }
        }
        return flag1 && flag2;
    }

    public static void main(String[] args) {
        TreeNode r1 = new TreeNode(1);
        TreeNode r2 = new TreeNode(2);

        TreeNode r3 = new TreeNode(1);

        r1.left = null;
        r1.right = r2;
        r2.left=null;
        r2.right = null;
        r3.left = null;
        r3.right = null;

        System.out.println(isSameTree(r1, r3));
    }
}
