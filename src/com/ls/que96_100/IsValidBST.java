package com.ls.que96_100;

import com.ls.utils.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：ls
 * @date ：2022/3/14 13:07
 * @description：...
 */
public class IsValidBST {
    public static boolean isValidBST(TreeNode root) {
        return (boolean)check(root).get(2);
    }

    public static List<Object> check(TreeNode node){
        List<Object> res = new ArrayList<>();
        if (node == null){
            res.add(Long.MAX_VALUE);
            res.add(Long.MIN_VALUE);
            res.add(true);
            return res;
        }

        List<Object> left = check(node.left);
        List<Object> right = check(node.right);

        if (!((boolean)left.get(2) && (boolean)right.get(2))){
            res.add(Long.MIN_VALUE);
            res.add(Long.MAX_VALUE);
            res.add(false);
            return res;
        }

        if (node.val <= (long)left.get(1) || node.val >= (long)right.get(0)){
            res.add(Long.MIN_VALUE);
            res.add(Long.MAX_VALUE);
            res.add(false);
            return res;
        }
        if (node.left==null)
            res.add((long)node.val);
        else res.add(left.get(0));
        if (node.right == null)
            res.add((long)node.val);
        else res.add(right.get(1));
        res.add(true);
        return res;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode t = new TreeNode(1);
        root.left=t;
        root.right = null;
        t.left = null;
        t.right = null;
        System.out.println(isValidBST(root));
    }
}
