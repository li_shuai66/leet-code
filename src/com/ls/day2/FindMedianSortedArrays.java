package com.ls.day2;

import com.mysql.cj.QueryResult;
import com.sun.corba.se.impl.ior.NewObjectKeyTemplateBase;

public class FindMedianSortedArrays {


    public static void main(String[] args) {
        int nums1[] = {1,2};
        int nums2[] = {3,4};
        System.out.println(findMedianSortedArrays(nums1,nums2));
    }


    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int term[] = new int[nums1.length+nums2.length];
        int m=0,n=0,i=0;
        for(m=0,n=0,i=0;m<nums1.length&&n<nums2.length;){
            if(nums1[m]<nums2[n]){
                term[i++] = nums1[m++];

            }else{
                term[i++] = nums2[n++];

            }
        }

        if(m<nums1.length){
            while (m<nums1.length)
                term[i++] = nums1[m++];
        }
        if(n<nums2.length){
            while (n<nums2.length)
                term[i++] = nums2[n++];
        }

        if((nums1.length+ nums2.length)%2==1){
            return (double)term[(nums1.length+ nums2.length)/2];
        }else {
            return ((double) term[(nums1.length+ nums2.length)/2-1] + (double)term[(nums1.length+ nums2.length)/2])/2;
        }
    }
}
