package com.ls.que71_75;

/**
 * @author ：ls
 * @date ：2021/11/29 11:00
 * @description：...
 */
public class SortColors {
    public void sortColors(int[] nums) {
        int a=0,b=0,c=0,i=0;
        for (i=0;i<nums.length;i++){
            if (nums[i]==0)
                a++;
            else if (nums[i]==1)
                b++;
            else c++;
        }

        for (i=0;i<a;i++)
            nums[i]=0;
        for (i=a;i<a+b;i++)
            nums[i]=1;
        for (i=a+b;i<a+b+c;i++)
            nums[i]=2;
    }

}
