package com.ls.que71_75;

/**
 * @author ：ls
 * @date ：2021/11/28 23:52
 * @description：...
 */
public class SetZeroes {
    public static void setZeroes(int[][] matrix) {
        int i,j;
        boolean flagRow=false;
        boolean flagCol=false;
        if (matrix.length == 1){
            for (i=0;i<matrix[0].length;i++)
                if (matrix[0][i]==0){
                    for (j=0;j<matrix[0].length;j++){
                        matrix[0][j]=0;
                    }
                    break;
                }
            return;
        }

        for (i=0;i<matrix.length;i++){

            for (j=0;j<matrix[0].length;j++){
                if (i==0 && matrix[i][j] == 0)
                    flagCol=true;
                if (j==0 && matrix[i][j] == 0)
                    flagRow = true;
                if (matrix[i][j] == 0){
                    matrix[i][0] = 0;
                    matrix[0][j] = 0;
                }
            }
        }


        for (i=1;i<matrix.length;i++){
            if (matrix[i][0] == 0)
                for (j=0;j<matrix[0].length;j++)
                    matrix[i][j] = 0;
        }

        for (i=1;i<matrix[0].length;i++){
            if (matrix[0][i]==0){
                for (j=0;j<matrix.length;j++){
                    matrix[j][i] = 0;
                }
            }
        }
        if (flagCol){
            for (j=0;j<matrix[0].length;j++)
                matrix[0][j] = 0;
        }

        if (flagRow){
            for (i=0;i<matrix.length;i++)
                matrix[i][0] = 0;
        }

    }

    public static void main(String[] args) {
        int[][] matrix = new int[][]{{1,2,3,4},{5,0,7,8},{0,10,11,12},{13,14,15,0}};
        setZeroes(matrix);
        for (int i=0;i<matrix.length;i++){
            for (int j=0; j<matrix[0].length;j++)
                System.out.print(matrix[i][j] + " ");
            System.out.println();
        }
    }
}
