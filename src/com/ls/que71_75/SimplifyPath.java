package com.ls.que71_75;

import jdk.nashorn.internal.ir.WhileNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：ls
 * @date ：2021/11/27 20:27
 * @description：...
 */
public class SimplifyPath {

    public static class MyFile{
        MyFile father;
        String name;
        public MyFile(MyFile father, String name){
            this.father = father;
            this.name = name;
        }
    }

    public static String simplifyPath(String path) {
        // 初始化root
        MyFile root = new MyFile(null, "");
        // 标记当前目录和父级目录
        MyFile now = root;
        // 将多个连续的 // 变为单个
        String simplePath = "";
        // 判断是否有文件存在
        boolean flag = false;
        for (String s: path.split("/")){
            if (!s.equals("")){
                flag = true;
                // 正常的字符类别有： 文件目录  .  ..  ... _
                if (s.charAt(0) == '.'){ // 是。 ，判断有几个
                    // 如果是一个.，那么这个s可以忽略
                    if (s.length() == 1)
                        continue;
                    else if (s.length() == 2){  // 两个点，就向上一层
                        // 在最顶层的基础上继续向上，就直接返回 /
                        if (now != root)
                            now = now.father;
                    }else {
                        // 当成普通的文件名
                        MyFile myFile = new MyFile(now, s);
                        now = myFile;
                    }
                }else{
                    // 当成普通的文件名
                    MyFile myFile = new MyFile(now, s);
                    now = myFile;
                }
            }
        }

        if (!flag || now == root)
            return "/";

        List<String> res = new ArrayList<>();
        while (now != null){
            res.add(now.name);
            now = now.father;
        }

        // 整理路径进行输出
        String s = "";
        for (int i=res.size()-2; i>=0;i--){
            s += "/" + res.get(i);
        }

        return s;
    }

    public static void main(String[] args) {
        System.out.println(simplifyPath("/../"));
    }
}
