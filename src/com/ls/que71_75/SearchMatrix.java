package com.ls.que71_75;

/**
 * @author ：ls
 * @date ：2021/11/29 0:45
 * @description：采用二分搜索
 */
public class SearchMatrix {

    public static boolean searchMatrix(int[][] matrix, int target) {
        int low = 0, high = matrix.length * matrix[0].length - 1;
        int middle = 0;
        int i,j;
        while (low < high){
            middle = (low + high) / 2;
            i = middle / matrix[0].length;
            j = middle % matrix[0].length - 1;

            if (matrix[i][j] == target)
                return true;

            if (matrix[i][j] < target){
                low = middle + 1;
            }else {
                high = middle - 1;
            }
        }
        return false;
    }

    public static void main(String[] args) {

    }
}
