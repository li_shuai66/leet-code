package com.ls.que121_125;

/**
 * @author ：ls
 * @date ：2022/4/1 18:38
 * @description：...
 */
public class MaxProfit {
    public int maxProfit(int[] prices) {
        if (prices.length==1)
            return 0;
        int max = prices[prices.length-1];
        int maxProfit = 0;

        for (int i=prices.length-2; i>=0; i--){
            max = Math.max(max, prices[i]);

            maxProfit = Math.max(maxProfit, max - prices[i]);
        }
        return maxProfit;
    }
}
