package com.ls.que121_125;

/**
 * @author ：ls
 * @date ：2022/4/5 13:48
 * @description：...
 */
public class IsPalindrome {
    public boolean isPalindrome(String s) {
        char[] chars = s.toCharArray();
        for (int i=0; i<chars.length; i++){
            if (!(chars[i]>='0' && chars[i] <='9' || chars[i]>='a' && chars[i]<='z' || chars[i]>='A' && chars[i]<='Z')) {
                chars[i] = ' ';
            }
        }
        s = new String(chars).replace(" ", "").toLowerCase();
        chars = s.toCharArray();
        for (int i=0; i<s.length()/2; i++){
            if (chars[i] != chars[s.length()-i-1])
                return false;
        }
        return true;
    }

    public static void main(String[] args) {
        String s = "A man, a plan, a canal: Panama";
        char[] chars = s.toCharArray();
        for (int i=0; i<chars.length; i++){
            if (!(chars[i]>='0' && chars[i] <='9' || chars[i]>='a' && chars[i]<='z' || chars[i]>='A' && chars[i]<='Z')) {
                chars[i] = ' ';
            }
        }
        s = new String(chars);
        s = s.replace(" ", "").toLowerCase();
//        String s = new String(a);
        System.out.println(s);
    }

}
