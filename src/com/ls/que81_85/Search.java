package com.ls.que81_85;

/**
 * @author ：ls
 * @date ：2021/12/3 9:57
 * @description：...
 */
public class Search {
    public boolean search(int[] nums, int target) {
        int l=0, r=0, k=-1, mid;
        for(int i=1;i<nums.length;i++){
            if (nums[i] < nums[i-1]){
                k=i-1;
                break;
            }
        }

        if (k==-1){
            l = 0;
            r = nums.length-1;
        }else if (target >= nums[0]){
            l = 0;
            r = k;
        }else {
            l = k+1;
            r = nums.length-1;
        }

        while (l <= r){
            mid = (l + r) / 2;
            if (nums[mid] == target){
                return true;
            }

            if (target > nums[mid]){
                l = mid + 1;
            }else
                r = mid - 1;
        }
        return false;
    }
}
