package com.ls.que81_85;

import com.ls.day2.ListNode;

/**
 * @author ：ls
 * @date ：2022/3/3 12:22
 * @description：...
 */
public class DeleteDuplicates {
    public static ListNode deleteDuplicates(ListNode head) {
        if (head == null) return null;
        ListNode p = head, q = head.next;
        while (q!=null){
            if (q.val == p.val){
                q = q.next;
            }else {
                p.next = q;
                p = q;
                q =  q.next;
            }
        }
        p.next = null;
        return head;
    }

    public static void main(String[] args) {
        ListNode n1 = new ListNode(1);
        ListNode n2 = new ListNode(1);
        ListNode n3 = new ListNode(2);
        ListNode n4 = new ListNode(3);
        ListNode n5 = new ListNode(3);
        n1.next = n2;
        n2.next = n3;
        n3.next = n4;
        n4.next = n5;
        n5.next = null;
        deleteDuplicates(n1);
    }
}
