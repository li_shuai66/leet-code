package com.ls.que81_85;

import com.ls.day2.ListNode;

/**
 * @author ：ls
 * @date ：2022/3/3 12:54
 * @description：...
 */
public class DeleteDuplicates2 {
    public ListNode deleteDuplicates(ListNode head) {
        if (head == null) return null;
        ListNode p = head, q = head, r = null;
        head = null;
        while (q!=null){
            while (q!=null && p.val == q.val){
                q = q.next;
            }
            if (p.next == q){
                if (head == null){
                    head = p;
                    r = p;
                }else {
                    r.next = p;
                    r = p;
                }
                r.next = null;
            }
            p = q;
        }
        return head;
    }
}
