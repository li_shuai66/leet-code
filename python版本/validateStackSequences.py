def validateStackSequences(pushed, popped):
    # 当前检测数据的坐标
    index = 0
    # 一个临时栈
    temp = []
    while len(pushed) != 0:
        # 对于pushed中的每一个数据来说，有两种操作，分别是放（入栈），不入（先入后出）
        val = pushed.pop(0)
        if (val == popped[index]):
            # 值相等了
            index += 1

            # 检查前面的元素
            while len(temp) != 0:
                if popped[index] == temp[-1]:
                    index += 1
                    temp.pop()
                else:
                    break
        else:
            # 值不相等
            temp.append(val)
    
    while len(temp) != 0:
        val = temp.pop()
        if val == popped[index]:
            index += 1
        else:
            return False
    
    return True

print(validateStackSequences([1,2,3,4,5], [4,3,5,1,2]))
